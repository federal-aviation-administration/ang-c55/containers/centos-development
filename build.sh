#!/bin/bash

#Usage ./build.sh <registry address> <container namespace path> <image name> <path containing Dockerfile>
#Example: ./build.sh registry.someurl.com:4567 gov/org/containers image-name/sub-name ./sub-name/sub-name
set -e

if [ -z ${1+x} ]; 
  then echo "Registry address has not been specified";
  exit 1
else 
  registry_address=$1
fi

if [ -z ${2+x} ];
  then echo "Container namespace/path has not been specified";
  exit 1
else
  namespace="$2"
fi

if [ -z ${3+x} ];
  then echo "Image name has not been specified";
  exit 1
else
  image_name="$3"
fi

if [ -z ${4+x} ];
  then echo "Path containing Dockerfile has not been specified";
  exit 1
else
  dockerfile_path="$4"
fi

full_image_path="$registry_address/$namespace/$image_name"

#cd to where the Dockerfile is located 
cd "$dockerfile_path"

echo "Building Dockerfile in $dockerfile_path and tagging image as $full_image_path"
docker build --pull --build-arg REGISTRY_ADDRESS=$registry_address --build-arg NAMESPACE=$namespace -t $full_image_path .
