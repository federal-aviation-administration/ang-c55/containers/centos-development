#!/bin/bash

#Usage ./push.sh <registry address> <container namespace path> <image name>
#Example: ./push.sh registry.someurl.com:4567 gov/org/containers image-name/sub-name
set -e

if [ -z ${1+x} ];
  then echo "Registry address has not been specified";
  exit 1
else
  registry_address=$1
fi

if [ -z ${2+x} ];
  then echo "Container namespace/path has not been specified";
  exit 1
else
  namespace="$2"
fi

if [ -z ${3+x} ];
  then echo "Image name has not been specified";
  exit 1
else
  image_name="$3"
fi

full_image_path="$registry_address/$namespace/$image_name"

#Use the version from the container label for the tag in the registry
version_tag=$(docker inspect --format='{{.ContainerConfig.Labels.version}}' $full_image_path)

#Check to see if a version tag is set before pushing the image to the registry
if [ "$version" = "<no value>" ]; then
	echo "No version label present at '.ContainerConfig.Labels.version'! Ensure version is set via LABEL before pushing"; 
	exit 1;
fi

docker tag  $full_image_path $full_image_path:$version_tag
docker push $full_image_path:latest
docker push $full_image_path:$version_tag
