#!/bin/bash

#Usage ./push_all.sh <registry address> <container namespace path>
#Example: ./push_all.sh registry.someurl.com:4567 gov/org/containers

set -e

if [ -z ${1+x} ];
  then echo "Registry address has not been specified";
  exit 1
else
  registry_address=$1
fi

if [ -z ${2+x} ];
  then echo "Container namespace/path has not been specified";
  exit 1
else
  namespace="$2"
fi

echo "Pushing images within $registry_address registry and $namespace path"
./push.sh "$@" "centos-development/oraclejdk/8"
./push.sh "$@" "centos-development/adoptopenjdk/8"
./push.sh "$@" "centos-development/adoptopenjdk/11"
